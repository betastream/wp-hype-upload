<?php 

namespace Betastream\HYPE;

/**
*	unzip zip file on media upload
*	@param int
*	@return int
*/ 
function add_attachment( $post_id ){
	$mime = get_post_mime_type( $post_id );
	
	if( $mime == 'application/zip' ){
		if( !defined('FS_METHOD') )
			define( 'FS_METHOD', 'direct' );

		add_filter( 'filesystem_method', __NAMESPACE__.'\filesystem_method', 10, 4 );

		WP_Filesystem();

		$zip_file_paths = zip_file_paths( $post_id );
		$unzip = unzip_file( $zip_file_paths['source_file'], $zip_file_paths['target_dir'] );

		if( $unzip === TRUE ){
			// update post meta?
		}
	}

	return $post_id;
}
add_action( 'add_attachment', __NAMESPACE__.'\add_attachment', 10, 1 );

/**
*	send script from hype package to editor when selected from upload
*	@param string default html markup for attachment
*	@param int attachment ID
*	@param array
*	@return string html markup for attachment
*/
function media_send_to_editor( $html = '', $post_id = 0, $attachment = array() ){
	$mime = get_post_mime_type( $post_id );

	if( $mime == 'application/zip' && function_exists('\sjr\get_tags_from_html') ){
		$hype_html = FALSE;
		$zip_file_paths = zip_file_paths( $post_id );

		if( !$zip_file_paths['is_external'] && file_exists($zip_file_paths['target_html']) ){
			$hype_html = file_get_contents( $zip_file_paths['target_html'] );
		} elseif( $zip_file_paths['is_external'] && ($response = wp_remote_get($zip_file_paths['target_html'])) ){
			$hype_html =  wp_remote_retrieve_body( $response );
		} 
		
		if( $hype_html ){
			// get the html from the body tag
			$body = \sjr\get_tags_from_html( $hype_html, 'body' );
			$html = \sjr\get_domli_html_inner( $body );

			// replace relative script src attribute with path to uploads
			$script_tags = \sjr\get_tags_from_html( $hype_html, 'script' );
			foreach( $script_tags as $tag ){
				$src = $tag->getAttribute( 'src' );
				$html = str_replace( $src, $zip_file_paths['target_url'].'/'.$src, $html );
			}
		}
	}

	return $html;
}
add_filter( 'media_send_to_editor', __NAMESPACE__.'\media_send_to_editor', 10, 3 );

/**
*	register meta box for atttachment edit screen
*/
function meta_box_add( $post ){
	$meta = get_post_meta( $post->ID, '_s3_hype_files', TRUE );
	
	if( !empty($meta) )
		add_meta_box( 's3-hype', 'S3 Hype files', __NAMESPACE__.'\render_meta_box', 'attachment', 'side', 'core' );
}
add_action( 'add_meta_boxes_attachment', __NAMESPACE__.'\meta_box_add' );

/**
*	render s3 hype files meta on attachment edit screen
*	@param WP_Post
*	@param array
*/
function render_meta_box( $post, $args ){
	global $as3cf;

	$s3_meta = get_post_meta( $post->ID, '_s3_hype_files', TRUE );

	$scheme = $as3cf->get_s3_url_scheme();
	$bucket = $as3cf->get_setting( 'bucket' );

	echo '<ul>';

	foreach( $s3_meta as $k=>$v )
		echo sprintf( '<li><a href="%s://%s/%s">%s</a></li>', $scheme, $bucket, esc_attr($v), esc_html(basename($v)) );

	echo '</ul>';
}