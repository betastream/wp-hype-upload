<?php

class WP_Filesystem_Direct_ZIP extends WP_Filesystem_Direct {
	// keep track of unzipped files
	protected $files = array();

	public function __construct( $arg ){
		parent::__construct( $arg );

		// action in Amazon / S3 plugin
		add_action( 'wpos3_post_upload_attachment', array($this, 'wpos3_post_upload_attachment'), 10, 2 );
	}

	/**
	*	keeps track of the files unzipped, to be uploaded to S3 in wpos3_post_upload_attachment()
	*	@param string $file     Remote path to the file where to write the data.
	*	@param string $contents The data to write.
	*	@param int    $mode     Optional. The file permissions as octal number, usually 0644
	*	@return bool False upon failure, true otherwise.
	*/
	public function put_contents( $file, $contents, $mode = false ){
		$ok = parent::put_contents( $file, $contents, $mode );

		if( $ok )
			array_push( $this->files, $file );
		
		return $ok;
	}

	/**
	*	saves the unzipped files to S3, and keeps track in `_s3_hype_files` post meta
	*	attached to `wpos3_post_upload_attachment` in Amazon / S3
	*	@param int
	*	@param
	*	@return int
	*/
	public function wpos3_post_upload_attachment( $post_id, $s3object ){
		global $as3cf;

		$bucket = $as3cf->get_setting( 'bucket' );
		$region = $as3cf->get_setting( 'region' );
		$s3_client = $as3cf->get_s3client( $region, FALSE );
		$s3_files = array();

		$args = array(
			'Bucket'       => $bucket,
			'ACL'          => Amazon_S3_And_CloudFront::DEFAULT_ACL,
			'CacheControl' => 'max-age=31536000',
			'Expires'      => date( 'D, d M Y H:i:s O', time() + 31536000 ),
		);

		foreach( $this->files as $file ){
			$args = array_merge( $args, array(
				'ContentType'  => mime_content_type( $file ),
				'Key'        => substr( $file, strlen(ABSPATH) ),
				'SourceFile' => $file,
			) );

			array_push( $s3_files, $args['Key'] );

			try {
				$s3_client->putObject( $args );
			} catch ( Exception $e ){
				// @TODO ??
			}
		}

		update_post_meta( $post_id, '_s3_hype_files', $s3_files );
	}
}
