<?php
/*
Plugin Name: 	Hype Upload
Plugin URI: 
Description:	
Author:			Eric Eaglstun
Version:		1.0
Author URI: 
License: 		GNU General Public License v2 or later
License URI: 	http://www.gnu.org/licenses/gpl-2.0.html

This file must be parsable by php 5.2
*/

register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );

require __DIR__.'/index.php';