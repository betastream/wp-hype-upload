# WP Hype Upload

Integration to upload Hype zip files to media library, and extract files within the post editor.  Has compatability with Amazon / S3 ( WP Offload S3 ).

## Usage

The hype project should be uploaded as a zip file to the media library, as would be for any normal image file.  The plugin will automatically unzip the file, and upload to S3, if WP Offload S3 is active.

From the post editor, the hype can be embedded by selecting the zip file from the Insert Media dialog.  The plugin will embed the script tags, usually found in in the index.html file of the Hype project instead of a link to the zip.