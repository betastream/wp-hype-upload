<?php 

namespace Betastream\HYPE;

if( is_admin() )
	require __DIR__.'/admin.php';

/**
*	force filesystem method to our custom WP_Filesystem_Direct_ZIP class
*	attached to `filesystem_method` filter in `add_attachment` action when mime is `application/zip`
*	@param string
*	@param bool
*	@param string
*	@param bool
*	@return string
*/
function filesystem_method( $method, $args, $context, $allow_relaxed_file_ownership ){
	$method = 'direct_zip';

	require_once ABSPATH.'/wp-admin/includes/file.php';
	require_once ABSPATH.'/wp-admin/includes/class-wp-filesystem-direct.php';
	require_once __DIR__.'/lib/class-wp-filesystem-direct-zip.php';

	return $method;
}

/**
*	gets url and file path information about an attachment
*	@param int
*	@return array
*/
function zip_file_paths( $post_id ){
	global $as3cf;

	$is_external = FALSE;

	$source_file = realpath( get_attached_file($post_id) );
	$source_url = wp_get_attachment_url( $post_id );

	$pathinfo = pathinfo( $source_file );
	$target_dir = $pathinfo['dirname'].'/'.$pathinfo['filename'].'/';
	wp_mkdir_p( $target_dir );

	$target_url = FALSE;
	
	$s3_meta = get_post_meta( $post_id, '_s3_hype_files', TRUE );
	if( !empty($s3_meta) && $as3cf instanceof \Amazon_S3_And_CloudFront ){
		$scheme = $as3cf->get_s3_url_scheme();
		$bucket = $as3cf->get_setting( 'bucket' );

		foreach( $s3_meta as $file ){
			if( pathinfo($file, PATHINFO_EXTENSION) == 'html' ){
				$target_html = sprintf( '%s://%s/%s', $scheme, $bucket, $file );
				$target_url = pathinfo( $target_html, PATHINFO_DIRNAME );

				$is_external = TRUE;
			}
		}
	} else {
		try{
			$dir_iterator = new \RecursiveDirectoryIterator( $target_dir );
			$iterator = new \RecursiveIteratorIterator( $dir_iterator, \RecursiveIteratorIterator::SELF_FIRST );
			$target_html = FALSE;

			$wp_upload_dir = wp_upload_dir();
			
			foreach( $iterator as $file ){
				if( $file->getExtension() == 'html' ){
					$target_html = $file->getPathname();
					$target_url = str_replace( $wp_upload_dir['basedir'], $wp_upload_dir['baseurl'], $file->getPath() );
				}
			}
		} catch( \Exception $e ){
			
		}
	}
	
	$return = array(
		'is_external' => $is_external,		// on filesystem or s3
		'source_file' => $source_file,		// full path to zip file
		'source_url' => $source_url,		// url to zip file
		'target_dir' => $target_dir,		// full path to unzipped file directory
		'target_html' => $target_html,		// full path to html file in directtory
		'target_url' => $target_url			// url to unzipped file directory, no trailing slash
	);
	
	return $return;
}